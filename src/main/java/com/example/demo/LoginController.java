package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sun.el.stream.Optional;

/**
 * @author hariom
 *
 */

@RestController
public class LoginController {
	
	@Autowired
	ServiceClass serviceClass;
	
	
	@Autowired
	UserLoginRepo repo;
	
	@Autowired
	JWTAuthBase jwtUtil;
	
	@Autowired
	AuthenticationManager aManager;
	
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	@RequestMapping(value = "/createUser", method=RequestMethod.POST, produces = "application/json")
    @CrossOrigin(origins = "http://localhost:4201")

	public LoginUser createUser(@RequestBody LoginUser loginUserObject )
	{	
		System.out.println("2");
		System.out.println(loginUserObject.firstName + loginUserObject.lastName + loginUserObject.email + loginUserObject.password);
		//instantiate the POJO class
		LoginUser entityClassObject = new LoginUser();
		entityClassObject.setFirstName(loginUserObject.firstName);
		entityClassObject.setLastName(loginUserObject.lastName);
		entityClassObject.setEmail(loginUserObject.email);
		System.out.println("3");

		String encodedPassword = bCryptPasswordEncoder.encode(loginUserObject.password);
		entityClassObject.setPassword(encodedPassword);
		System.out.println("4");

		entityClassObject.setRole("ROLE_"+loginUserObject.role);
		
		
		
		//call the service Class to create the User
		boolean response = serviceClass.createUserInDB(entityClassObject);

		
		//check for validations
		if(response == true)
		{
			return loginUserObject;
			
		}
		
		else
			return null;
		
	}

	
	@RequestMapping(value = "/authenticate", method=RequestMethod.POST, produces = "application/json")
    @CrossOrigin(origins = "http://localhost:4201")

    public StoreToken generateToken(@RequestBody AuthenticationRequest authRequest ) throws Exception
    {	
		System.out.println(authRequest.getEmail()+authRequest.getPassword());
    	try {
    	aManager.authenticate(

    			new UsernamePasswordAuthenticationToken(authRequest.getEmail(), authRequest.getPassword())
    	   

    	);
    	
    	}catch(Exception e)
    	{
    		throw new Exception("Invalid Username and password");

    	}
     	StoreToken token = new StoreToken();
    	token.setToken(jwtUtil.generateToken(authRequest.getEmail()));
    	return token;
    }
	

	
	@RequestMapping(value = "/test", method=RequestMethod.GET, produces = "application/json")

	public String test()
	{
		return "hi man!!";
	}
	
	@RequestMapping(value = "/testAPI", method=RequestMethod.GET, produces = "application/json")
   
    public String testAPI()
    {
        return "You are logged In !!";
    }
    

	
	
	
}
