package com.example.demo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLoginRepo extends JpaRepository <LoginUser, Integer> {

	    @Query(value = "SELECT * FROM CLASSMASTER_LOGIN_TABLE where EMAIL=:email", nativeQuery = true)
	    public Optional<LoginUser> findIdByEmail(@Param ("email") String email);
}
