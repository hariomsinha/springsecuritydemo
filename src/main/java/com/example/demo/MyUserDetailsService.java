package com.example.demo;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	UserLoginRepo repo;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		Optional <LoginUser> checkDB = repo.findIdByEmail(email);
		System.out.println(email + "checked");
		System.out.println(checkDB + "checked");
		checkDB.orElseThrow(()-> new UsernameNotFoundException( "Username not found : "+ email));
		
		return checkDB.map(MyUserDetails::new).get();
		
		
	}

		
}
