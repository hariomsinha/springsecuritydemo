package com.example.demo;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class MyUserDetails implements UserDetails {

	private int id;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private List<GrantedAuthority> roles;
	
	public MyUserDetails(LoginUser data)
	{
		this.id = data.id;
		this.firstname = data.getFirstName();
		this.lastname = data.getLastName();
		this.email = data.getEmail();
		this.password = data.getPassword();
		this.roles = Arrays.stream(data.getRole().split(","))
						.map(SimpleGrantedAuthority::new)
						.collect(Collectors.toList());
	}
	
	public MyUserDetails()
	{
		
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
			return roles;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	
	
}
