package com.example.demo;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class JWTFilter  extends OncePerRequestFilter{
	
	@Autowired
	private JWTAuthBase jwtutil;
	
	@Autowired
	private MyUserDetailsService service;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException
	{
	String AuthorizationHeader = request.getHeader("Authorization");

	String token = null;
	String username =null;
	System.out.print("Aheader : "+AuthorizationHeader);
	if(AuthorizationHeader !=null && AuthorizationHeader.startsWith("Bearer "))
	{
		System.out.print(AuthorizationHeader);

		token = AuthorizationHeader.substring(7);
		System.out.print("AHeader2 : "+token);
		username = jwtutil.extractUsername(token);


	
	}
	
	if(username!=null && SecurityContextHolder.getContext().getAuthentication() == null)
	{
		System.out.println("1");
		UserDetails userDetails = service.loadUserByUsername(username);
		if(jwtutil.validateToken(token, userDetails))
		{
			 UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                     new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
             usernamePasswordAuthenticationToken
                     .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
             SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
		}
	}
	
	filterChain.doFilter(request, response);
	
	
	
	
	}
	

}

	
